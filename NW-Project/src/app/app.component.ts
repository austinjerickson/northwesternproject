import { Component, OnInit, ViewChild } from '@angular/core';
import {MatSort} from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { map } from 'rxjs/operators';

import { CharacterDataService, Character } from './shared';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public dataSource: MatTableDataSource<Character>;
  public displayedColumns: Array<string>;
  public searchText: string;
  public title: string;

  @ViewChild(MatSort, {static: true}) sort: MatSort;

  public constructor(private characterDataService: CharacterDataService) {
    this.displayedColumns = [
      'name',
      'role',
      'house',
      'school',
      'species'
    ];
    this.searchText = '';
    this.title = 'Character Search';
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  public ngOnInit(): void {
    this.sort.sort({
      id: 'name',
      start: 'asc',
      disableClear: false
    });

    this.characterDataService
      .getCharacters()
      .pipe(
        map((characters) => {
          this.dataSource = new MatTableDataSource(characters);
          this.dataSource.sort = this.sort;
        })
      )
      .subscribe();
  }

  public searchTextChange(search: string): void {}
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { Character } from '../models';

interface CharacterResponse {
  Characters: Array<Character>;
}

@Injectable({
  providedIn: 'root'
})
export class CharacterDataService {
  public constructor(
    private http: HttpClient
  ) { }

  public getCharacters(): Observable<Array<Character>> {
    // tslint:disable-next-line:max-line-length
    return this.http.get<Array<Character>>('https://www.potterapi.com/v1/characters?key=$2a$10$vL5aPWiS6asuz4cbsJb3Dekko005ERQaDwjFmCa4kZFa/TFO/lO0O');
  }
}
